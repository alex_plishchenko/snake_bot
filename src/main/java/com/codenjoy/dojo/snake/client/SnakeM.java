package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.Random;

/**
 * Created by Plishchenko1 on 07.12.2015.
 */
public class SnakeM  {

    private Board _border;
    private char _field[][];
    private  YourSolver _solver;

    private boolean _appleRandom = false;

    public void Run() throws InterruptedException {

      //  while (stepNext() == false){
               // RandomDice();

            Thread.sleep(1000);

            System.out.println(_border.toString());


            stepNext(1);
        moveUp();

          //  stepNext();
          //  stepNext();
           System.out.println(_border.toString());

            String answer  = _solver.get(_border);

            randomAllpe();

          /*  if(answer == Direction.RIGHT.toString()){
                moveRight();
                continue;
            }
            if(answer == Direction.LEFT.toString()){
                moveLeft();
                continue;
            }
            if(answer == Direction.UP.toString()){
                moveUp();
                continue;
            }
            if(answer == Direction.DOWN.toString()){
                moveDown();
                continue;*/
           // }
      //  }
    }

    public SnakeM(Board b, YourSolver solver){
        _border = b;
        _field = _border.getField();
        _solver = solver;
    }

    public boolean stepNext(int nStep) {
        boolean res = false;
        for(int i = 0; i < nStep; i++){
            res = stepNext();
            if(stepNext() == true) {return res;};
        }
        return res;
    }

    public boolean stepNext(){

        Point pHead = _border.getHead();
        Point pTall  = _border.getSnake().get(_border.getSnake().size()-1);

        boolean isApple = false;

        try {

            switch (_border.getSnakeDirection()) {
                case RIGHT: {
                    if (pHead.getX() + 1 == 15) {
                        return true;
                    }
                    if (_field[pHead.getX() + 1][pHead.getY()] == Elements.GOOD_APPLE.ch()) {
                        isApple = true;
                    }else {
                        _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
                        _field[pTall.getX()][pTall.getY()] = Elements.NONE.ch();
                    }

                    _field[pHead.getX() + 1][pHead.getY()] = Elements.HEAD_RIGHT.ch();
                    break;
                }
                case LEFT: {
                    if (pHead.getX() - 1 == 0) {
                        return true;
                    }

                    if (_field[pHead.getX() - 1][pHead.getY()] == Elements.GOOD_APPLE.ch()) {
                        isApple = true;
                    }else {
                        _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
                        _field[pTall.getX()][pTall.getY()] = Elements.NONE.ch();
                    }

                    _field[pHead.getX() - 1][pHead.getY()] = Elements.HEAD_LEFT.ch();
                    break;
                }
                case UP: {
                    if (pHead.getY() - 1 == 0) {
                        return true;
                    }
                    _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();

                    if (_field[pHead.getX()][pHead.getY() + 1] != Elements.GOOD_APPLE.ch()) {
                        _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
                    }else {_appleRandom = true;}

                    _field[pHead.getX()][pHead.getY() - 1] = Elements.HEAD_UP.ch();
                    break;
                }
                case DOWN: {
                    if (pHead.getY() + 1 == 15) {
                        return true;
                    }
                    _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();

                    if (_field[pHead.getX()][pHead.getY() + 1] != Elements.GOOD_APPLE.ch()) {
                        _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
                    }else {_appleRandom = true;}

                    _field[pHead.getX()][pHead.getY() + 1] = Elements.HEAD_DOWN.ch();
                    break;
                }
            }

            if(isApple == false) {
                _appleRandom = true;
            }

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ArrayIndexOutOfBoundsException stepNext");
            return  true;
        }
        return false;
    }

    public boolean moveUp() {

        if(_border.getSnakeDirection() == Direction.UP) { return stepNext();}

        Point pHead = _border.getHead();
        Point pTall  = _border.getSnake().get(_border.getSnake().size()-1);

        try {

            if(_field[pHead.getX()][pHead.getY() - 1] != Elements.GOOD_APPLE.ch()){
                _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
            }else {_appleRandom = true;}

            _field[pHead.getX()][pHead.getY()-1] = Elements.HEAD_UP.ch();

            if( _field[pHead.getX() - 1][pHead.getY()] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }else if(_field[pHead.getX() + 1][pHead.getY()] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }

        }
        catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ArrayIndexOutOfBoundsException moveUp");
            return  true;
        }
        return false;
    }

    public boolean moveDown() {

        if (_border.getSnakeDirection() == Direction.DOWN) {
            return stepNext();
        }

        Point pHead = _border.getHead();
        Point pTall = _border.getSnake().get(_border.getSnake().size() - 1);

        try {

            if (_field[pHead.getX()][pHead.getY() + 1] == Elements.GOOD_APPLE.ch()) {
                _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
            }else {_appleRandom = true;}

            _field[pHead.getX()][pHead.getY() + 1] = Elements.HEAD_DOWN.ch();

            if (_field[pHead.getX() - 1][pHead.getY()] != ' ') {
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            } else if (_field[pHead.getX() + 1][pHead.getY()] != ' ') {
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ArrayIndexOutOfBoundsException moveDown");
                return  true;
        }
        return false;
    }

    public boolean moveLeft() {

        if(_border.getSnakeDirection() == Direction.LEFT) { return stepNext();}

        Point pHead = _border.getHead();
        Point pTall  = _border.getSnake().get(_border.getSnake().size()-1);

        try {

            if(_field[pHead.getX()-1][pHead.getY()] != Elements.GOOD_APPLE.ch()){
                _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
            }else {_appleRandom = true;}

            _field[pHead.getX() - 1][pHead.getY()] = Elements.HEAD_LEFT.ch();

            if( _field[pHead.getX()][pHead.getY() - 1] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }else if(_field[pHead.getX()][pHead.getY() + 1] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ArrayIndexOutOfBoundsException moveLeft");
                return  true;
            }
        return false;
    }

    public boolean moveRight() {

        if(_border.getSnakeDirection() == Direction.RIGHT) { return stepNext();}

        Point pHead = _border.getHead();
        Point pTall  = _border.getSnake().get(_border.getSnake().size()-1);

        if(pHead.getX() + 1 == 15) { return true; }

        try {

            if(_field[pHead.getX() + 1][pHead.getY()] != Elements.GOOD_APPLE.ch()){
                _field[pTall.getX()][pTall.getY()] = Elements.TAIL_END_DOWN.ch();
            }else {_appleRandom = true;}

            _field[pHead.getX() + 1][pHead.getY()] = Elements.HEAD_RIGHT.ch();

            if( _field[pHead.getX()][pHead.getY() + 1] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }else if(_field[pHead.getX()][pHead.getY() - 1] != ' '){
                _field[pHead.getX()][pHead.getY()] = Elements.TAIL_END_DOWN.ch();
            }

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ArrayIndexOutOfBoundsException  moveRight");
            return  true;
        }
        return false;
    }

    private void randomAllpe(){

        if(_appleRandom == false) { return;}
        int x = 0;
        int y = 0;

        while (true)
        {
            x = new Random().nextInt(10) + 2;
            y = new Random().nextInt(10) + 2;
            if(_field[x][y] == ' '){
                _field[x][y] = Elements.GOOD_APPLE.ch();
                _appleRandom = false;
                return;
            }
        }
    }
}
