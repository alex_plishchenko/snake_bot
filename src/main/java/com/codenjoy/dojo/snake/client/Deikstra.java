package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.snake.model.Elements;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Bogdan on 03.12.2015.
 */
public class Deikstra {
    private int INF = Integer.MAX_VALUE / 2;


    private int n;

    private ArrayList<Integer> adj[];
    private ArrayList<Integer> weight[];
    private boolean used[];
    private int dist[];

    private int pred[];
    private char field[][];

    private int start;
    private int finish;

    Deikstra(){

    }
    Deikstra(char [][] field){
        this.field = field;
    }

    public void setField(char [][] field) {
        this.field = field;
    }

    private void showField(char[][] ar){
        System.out.println("\n showField");
        for(int i = 0; i < ar.length; i++){
            for(int j= 0; j < ar.length; j++)  System.out.print( ar[j][i] + " ");
            System.out.println("");
        }
        System.out.println("------------------------");
    }

    private void showTable(int[][] ar){
        System.out.println("\n showTable");
        for(int i = 0; i < ar.length; i++){
            for(int j= 0; j < ar.length; j++)  System.out.print( ar[j][i] + " ");
            System.out.println("");
        }
    }

    private void showPred(int[] ar){
        System.out.println("\n show Pred");
        for(int i = 0; i < ar.length; i++) System.out.print( ar[i] + " ");
            System.out.println("");
    }
    public static char[][] rotateClockwise(char[][] arg) {
        int dimMatrix =  arg.length;
        char [][] resRot = new char[dimMatrix][dimMatrix];
        for (int i = 0; i < dimMatrix; i++){
            for (int j = 0; j < dimMatrix; j++){
                resRot[j][dimMatrix - i - 1] = arg[i][j];
            }
        }

        return resRot;
    }

    private int[][] codeField(char[][] field){

       // showField(field);
     //   field = rotateClockwise(field);
     //   showField(field);

        int [][] codeArr = new int [field.length][field.length];
        int count = 0;
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field.length; y++) {
                char ch = field[x][y];
                if (ch == Elements.TAIL_LEFT_UP.ch() ||
                        ch == Elements.TAIL_END_RIGHT.ch() ||
                        ch == Elements.TAIL_END_LEFT.ch() ||
                        ch == Elements.TAIL_END_DOWN.ch()||
                        ch == Elements.TAIL_VERTICAL.ch()||
                        ch == Elements.TAIL_END_UP.ch() ||
                        ch == Elements.BAD_APPLE.ch() ||
                        ch == Elements.TAIL_HORIZONTAL.ch() ||
                        ch == Elements.TAIL_RIGHT_UP.ch()||
                        ch == Elements.TAIL_LEFT_DOWN.ch()||
                        ch == Elements.TAIL_RIGHT_DOWN.ch()||
                        ch == Elements.BREAK.ch()){
                    codeArr[x][y] = -1;
                    count++;
                } else if (ch == Elements.HEAD_DOWN.ch()||
                        ch == Elements.HEAD_RIGHT.ch() ||
                        ch == Elements.HEAD_LEFT.ch() ||
                        ch == Elements.HEAD_UP.ch()){
                    codeArr[x][y] = count;
                    start = count++;
                } else if (ch == Elements.GOOD_APPLE.ch()){
                    codeArr[x][y] = count;
                    finish = count++;
                } else {codeArr[x][y] = count++;}
            }
        }
        return codeArr;
    }

    private void initialData(int [][] codeArr){
        int iter = 0;
        n = codeArr.length*codeArr.length;

        adj = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            adj[i] = new ArrayList<Integer>();
        }

        weight = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            weight[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < codeArr.length; i++) {
            for (int j = 0; j < codeArr.length; j++) {
                if (codeArr[i][j] == -1){
                    iter++;
                    continue;
                } else {
                    if (codeArr[i-1][j] != -1){
                        adj[iter].add(codeArr[i-1][j]);
                        weight[iter].add(1);
                    } if (codeArr[i+1][j] != -1){
                        adj[iter].add(codeArr[i+1][j]);
                        weight[iter].add(1);
                    } if (codeArr[i][j-1] != -1){
                        adj[iter].add(codeArr[i][j-1]);
                        weight[iter].add(1);
                    } if (codeArr[i][j+1] != -1){
                        adj[iter].add(codeArr[i][j+1]);
                        weight[iter].add(1);
                    } iter++;

                }
            }
        }

        used = new boolean[n];
        Arrays.fill(used, false);

        pred = new int[n];
        Arrays.fill(pred, -1);

        dist = new int[n];
        Arrays.fill(dist, INF);
    }

    public void dejkstra(int s) {
        dist[s] = 0;
        for (int iter = 0; iter < n; iter++) {
            if (adj[iter] == null){continue;}
            int v = -1;
            int distV = INF;

            for (int i = 0; i < n; ++i) {
                if (adj[i] == null){continue;}
                if (used[i]) {
                    continue;
                }
                if (distV < dist[i]) {
                    continue;
                }
                v = i;
                distV = dist[i];
            }

            for (int i = 0; i < adj[v].size(); i++) {

                int u = adj[v].get(i);
                int weightU = weight[v].get(i);

                if (dist[v] + weightU < dist[u]) {
                    dist[u] = dist[v] + weightU;
                    pred[u] = v;
                }
            }

            used[v] = true;
        }

    }

    public int[] nextStep(){
        int [] buf = new int[2];
        int v = 0, bufv = 0;
        int[][] codeArr = codeField(field);

        //showTable(codeArr);
       // System.out.println("node: \n");

        initialData(codeArr);
        dejkstra(start);

      //  showPred(pred);
        System.out.println("-----------\n");

        v = finish;
        while (v != start ){
            bufv = v;
            v = pred[v];
            System.out.print( v + " ");
            if (v == -1) break;
        }

        System.out.println("\n-----------\n");

        for (int i = 0; i < codeArr.length; i++) {
            for (int j = 0; j < codeArr.length; j++) {
                if (codeArr[i][j] == bufv) {
                    buf[0] = i;
                    buf[1] = j;
                } else {/*do nothing;*/}

            }
        }

        System.out.println("Next step: " + buf[0] + ":" + buf[1]);
        System.out.println("next node: " +  v + " -> " + bufv);

       return  buf;
    }



}
