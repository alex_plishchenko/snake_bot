/**
 */


public class Table {

  static  public  int[][] table;

    public static void setBoard(char[][]bord){
        int h = bord.length;
        int w = bord[0].length;

        table = new int[h][w];
        initTab(table, bord);
    }

    public static int[][] getTable(){
        return  table;
    }

    public enum TAIL{
        TAIL_END_DOWN('╙'),
        TAIL_END_LEFT('╘'),
        TAIL_END_UP('╓'),
        TAIL_END_RIGHT('╕'),
        TAIL_HORIZONTAL('═'),
        TAIL_VERTICAL('║'),
        TAIL_LEFT_DOWN('╗'),
        TAIL_LEFT_UP('╝'),
        TAIL_RIGHT_DOWN('╔'),
        TAIL_RIGHT_UP('╚');

        private char name;

        TAIL(char name){ this.name = name; }
    }

    public static  Boolean isTail(char ch)
    {
        TAIL[] t = TAIL.values();
        for (TAIL i: t) {
            if( i.name == ch) { return  true; }
        }
        return  false;
    }

    // temp
    public static  void show()
    {
        for(int[] i: table){
            for (int j: i){ System.out.print(j + " "); }
            System.out.print('\n');
        }
    }

    public static  void initTab(int[][] table, char[][]bord)
    {
        //final char APPLE = '☺';
        //final int priorityApple = 1;

        final char BAD_APPLE = '☻';
        final int priorityBadApple = -1;

        final char BREAK = '☼';
        final int priorityBreak = -1;

        final char BODY = 'B';
        final int priorityBody = -1;

        final int priorityTail = -1;

        int indexNode = 0;
        char charBord;

        for(int i = 0; i < table.length; i++){
            for (int j = 0; j < table[i].length; j++){
                charBord = bord[i][j];
                switch (charBord)
                {
                    //case apple: { table[i][j]  = priorityApple; ++indexNode; continue; }
                    case BAD_APPLE: {   table[i][j] = priorityBadApple; ++indexNode; continue; }
                    case BODY:      {   table[i][j] = priorityBody;     ++indexNode; continue; }
                    case BREAK:     {   table[i][j] = priorityBreak;    ++indexNode; continue; }
                }
                if(isTail(charBord) == true) { table[i][j] = priorityTail;   ++indexNode; continue;}
                table[i][j] = ++indexNode;
            }
        }
    }
}
